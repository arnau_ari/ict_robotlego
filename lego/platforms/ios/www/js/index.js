var app = {

    initialize: function() {
        this.bindEvents();
    },

    // Application Constructor
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        document.addEventListener('initApp', app.onInitApp, true);
        document.getElementById('start').addEventListener('click',app.onInit, false);
    },

    onDeviceReady: function() {
    },

    onInitApp: function() {
        document.getElementById("start").href = "start.html";
    },

    onInit: function () {
        app.throwStartEvent();
    },

    throwStartEvent: function () {
        var event = new Event ('initApp');
        document.getElementById("start").dispatchEvent(event);
    }
};

app.initialize();