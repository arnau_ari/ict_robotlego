function upAction() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "https://apitorobot.com/up", false);
    xhttp.send();
    alert("Robot moves up");
}
function leftAction() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "https://apitorobot.com/left", false);
    xhttp.send();
    alert("Robot moves left");
}
function rightAction() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "https://apitorobot.com/right", false);
    xhttp.send();
    alert("Robot moves right");
}
function downAction() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "https://apitorobot.com/down", false);
    xhttp.send();
    alert("Robot moves down");
}